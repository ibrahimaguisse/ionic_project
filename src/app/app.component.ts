import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthGuardServiceService } from 'src/app/auth-guard-service.service';
import { UserService } from 'src/app/components/user.service';
import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { LoginGuardService } from './login-guard.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Articles',
      url: '/articles',
      icon: 'list'
    },
    {
      title: 'Panier',
      url: '/panier',
      icon: 'list'
    },    
    {
      title: 'Mes commandes',
      url: '/orders',
      icon: 'list'
    },
    {
      title: 'Profil',
      url: '/profil',
      icon: 'person'
    },
  ];

  public appPagesGuest = [
    {
      title: 'Accueil',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Login',
      url: '/login',
      icon: 'list'
    },
    {
      title: 'S\'inscrire',
      url: '/register',
      icon: 'list'
    },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private userService: UserService,
    private authGuard: AuthGuardServiceService,
    private loginGuard: LoginGuardService,
    private router: Router,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  public logout() {
    Cookie.delete("token");
    this.userService.user = null;
    this.authGuard.isLogin = false;
    this.loginGuard.isLogin = false;
    this.router.navigate(['/home']);
  }
}
