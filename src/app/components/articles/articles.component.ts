import { Component, OnInit } from "@angular/core";
import { ArticlesService } from "./articles.service";
import { Router, NavigationExtras } from "@angular/router";
import { OrderService } from "../order/order.service";
import { UserService } from "../user.service";
import {
    Validators,
    FormControl,
    FormGroup,
    FormBuilder,
} from "@angular/forms";
import { ToastController } from '@ionic/angular';
import { CategoriesService } from '../categories.service';

@Component({
    selector: "app-articles",
    templateUrl: "./articles.component.html",
    styleUrls: ["./articles.component.scss"],
})
export class ArticlesComponent implements OnInit {
    articles: any[];

    articlesCopy: any[];

    categories: any[];

    order: any;

    user: any;

    formOrderArticle: FormGroup;

    toast: any;

    constructor(
        private articleService: ArticlesService,
        private orderService: OrderService,
        private userService: UserService,
        private router: Router,
        private formBuilder: FormBuilder,
        public toastController: ToastController,
        private categoriesService: CategoriesService
    ) {
        this.formOrderArticle = formBuilder.group({
            quantity: ['', Validators.required],
        });
    }

    ngOnInit() {
        this.getArticles();
        this.getCategories();
        this.user = this.userService.user;
    }

    ionViewWillEnter() {
        this.getArticles();
    }

    getArticles(): void {
        this.articleService.getArticles().subscribe((articles) => {
            articles.map(function (e) {
                e.img = "data:image/jpeg;base64," + e.img;
            });
            this.articles = articles;
            this.articlesCopy = articles
        });
    }

    getCategories(): void {
      this.categoriesService.getCategories().subscribe((categories) => {
          this.categories = categories;
      });
  }

    getArticle(article: any): void {
        let navigationExtras: NavigationExtras = {
            state: {
                article: article,
            },
        };
        this.router.navigate(["articles/" + article.id], navigationExtras);
    }

    addOrderArticle(article: any, quantity): void {

        if(this.formOrderArticle.valid){

          this.orderService.getOrderDraftByUser(this.user.id).subscribe((order: any) =>{

            let formData = {
              "orderId": order.id,
              "articleId": article.id,
              "quantity": quantity
            }
      
            this.orderService.addOrderArticles(formData).subscribe((data:any) => {
                console.log("OrderArticle save");
            });

          });

          this.toast = this.toastController.create({
            message: 'L\'article a été ajouté à votre panier',
            duration: 2000
          }).then((toastData)=>{
            toastData.present();
          });

        }else{
          this.toast = this.toastController.create({
            message: 'Le champ quantité est vide',
            duration: 2000
          }).then((toastData)=>{
            toastData.present();
          });
        }
    }

    filterBy(id:any){
      if(id != 'all'){
        this.articles = this.articlesCopy.filter(article => article.category.id == id);
      }else{
        this.articles = this.articlesCopy;
      }
    }

}
