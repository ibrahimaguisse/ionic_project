import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { AuthGuardServiceService } from 'src/app/auth-guard-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { LoginGuardService } from 'src/app/login-guard.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private authGuard: AuthGuardServiceService, private loginGuard: LoginGuardService, private http: HttpClient) { }

  ngOnInit() {
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
  };

  login(email: string, password: string){
    let formData = {
      "email" : email,
      "password": password
    }
    this.userService.login(formData)
    .subscribe(data => {
      if(data.token){
        this.userService.user = data.user;
        Cookie.set('token', data.token);
        this.authGuard.isLogin = true;
        this.loginGuard.isLogin = true;
        this.router.navigate(['/home']);
      }
    });
  }

}
