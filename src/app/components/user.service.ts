import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';

let token = Cookie.get("token");

const httpOptionsUpdate = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'auth': token })
};


@Injectable({
  providedIn: 'root'
})
export class UserService {
  

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
  };

  user;

  userUrl = 'http://localhost:3000/';

  constructor(private http: HttpClient) { }

  login(formData: any): Observable<any> {
    return this.http.post(this.userUrl + 'auth/login', formData, this.httpOptions);
  }

  register(formData: any): Observable<any> {
    return this.http.post(this.userUrl + 'user', formData, this.httpOptions);
  }

  update(formData:any){
    return this.http.patch(this.userUrl + 'user/'+this.user.id, formData, httpOptionsUpdate);
  }

  changePassword(formData:any){
    return this.http.post(this.userUrl + 'auth/change-password', formData, httpOptionsUpdate);
  }

}
