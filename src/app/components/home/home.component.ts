import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/components/user.service';
import { AuthGuardServiceService } from 'src/app/auth-guard-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  user;

  constructor(private userService: UserService, private guard: AuthGuardServiceService) {}

  ngOnInit() {
    this.user = this.userService.user;
  }

}
