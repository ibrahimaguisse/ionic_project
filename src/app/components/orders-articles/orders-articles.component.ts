import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { getOrCreateNodeInjectorForNode } from '@angular/core/src/render3/di';
import { OrderService } from '../order/order.service';

@Component({
    selector: "app-orders-articles",
    templateUrl: "./orders-articles.component.html",
    styleUrls: ["./orders-articles.component.scss"],
})
export class OrdersArticlesComponent implements OnInit {
    orderId: any;

    order:any;

    orderArticles:any[];

    constructor(private route: ActivatedRoute, private router: Router, private orderService: OrderService) {
        this.route.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.orderId = this.router.getCurrentNavigation().extras.state.order.id;
            }
        });
    }

    ngOnInit() {
      this.getOrder();
    }

    getOrder(){
      this.orderService.getOrder(this.orderId).subscribe((order:any) => {
        order.orderArticles.map(function(e){
          e.article.img = "data:image/jpeg;base64," + e.article.img;
        })
        var date = new Date(order.deliveryDate);
        order.deliveryDate = date.toISOString().split('T')[0] 
        this.order = order;
        this.orderArticles = order.orderArticles;
      });
    }

}
