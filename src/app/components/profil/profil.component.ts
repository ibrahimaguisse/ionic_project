import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss'],
})
export class ProfilComponent implements OnInit {

  user;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.user = this.userService.user;
  }

  updateUser(email:any, oldPassword:any, newPassword:any, mobilePhoneNumber:any, landlinePhoneNumber:any){

    let formData = {
      "email":email,
      "mobilePhoneNumber":mobilePhoneNumber,
      "landlinePhoneNumber":landlinePhoneNumber
    }

    this.userService.update(formData).subscribe((res:any)=>{
      //toast
    });

    if(oldPassword.length != 0 && newPassword != 0){
      let formData = {
        "oldPassword": oldPassword,
        "newPassword": newPassword
      }
      this.userService.changePassword(formData).subscribe((res:any)=>{
        //toast
      });
    }
  
  }

}
