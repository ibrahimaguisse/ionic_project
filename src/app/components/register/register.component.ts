import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { AuthGuardServiceService } from 'src/app/auth-guard-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private guard: AuthGuardServiceService, private http: HttpClient) { }

  ngOnInit() {
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
  };

  userUrl = 'http://localhost:3000/user';

  register(firstname: string, lastname:string,  email: string, password: string, passwordConf: string, mobilePhoneNumber: number, landlinePhoneNumber:number, civility:string, dateOfBirth: Date) {
    
    let formBody = {
      "firstname":firstname, 
      "lastname":lastname, 
      "email":email, 
      "password":password, 
      "role":"USER", 
      "mobilePhoneNumber":mobilePhoneNumber, 
      "landlinePhoneNumber":landlinePhoneNumber, 
      "civility":civility, 
      "dateOfBirth":dateOfBirth,
      "type":"application"
    }

    this.userService.register(formBody)
    .subscribe((data:any) => {
      if(data.token){
        Cookie.set('token', data.token);
        this.userService.user = data.user;
        this.guard.isLogin = true;
        this.router.navigate(['/home']);
      }
     }, error => {
      console.log(error);
    });
  }


}
