import { Component, OnInit } from '@angular/core';
import { OrderService } from "../order/order.service";
import { UserService } from "../user.service";
import { Router, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss'],
})
export class PanierComponent implements OnInit {

  orderArticles:any;
  
  order:any;

  user:any;

  price: number = 0;

  priceHt: number = 0;

  constructor(private orderService: OrderService, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.user = this.userService.user;
    this.getOrder();
  }

  getOrder(){

    this.orderService.getOrderDraftByUser(this.user.id).subscribe((order: any) =>{

      let price = 0;

      let priceHt = 0;

      if(typeof order.orderArticles != 'undefined'){

        order.orderArticles.forEach((e:any) => {
          price += e.article.price * (1 + e.article.tva / 100) * e.quantity;
          priceHt += e.article.price * e.quantity;
        });

        this.price = price;

        this.priceHt = priceHt;

        order.orderArticles.map(function(e:any){
          e.article.img = "data:image/jpeg;base64," + e.article.img; 
        });

        this.orderArticles = order.orderArticles;

        this.order = order;

      }

    });

  }

  deleteOrderArticles(id: any): void{

    this.orderService.deleteOrderArticles(id).subscribe((res: any) =>{
      this.ngOnInit();
    });

  }

  editOrderArticles(orderArticle: any, quantity: any): void{

    let formData = {
      "quantity": quantity,
      "orderId": this.order.id,
      "articleId": orderArticle.article.articleId
    };

    this.orderService.editOrderArticles(formData, orderArticle.id).subscribe((res: any) =>{
      // Toast
      this.ngOnInit();
    });

  }

  finishOrder(): void {
    let navigationExtras: NavigationExtras = {
        state: {
            order: this.order,
            orderArticles: this.orderArticles,
            price: this.price,
            priceHt: this.priceHt
        },
    };
    this.router.navigate(["finishOrder"], navigationExtras);
}

}
