import { Component, OnInit } from "@angular/core";
import { OrderService } from "../order/order.service";
import { ActivatedRoute, Router, NavigationExtras } from "@angular/router";

@Component({
    selector: "app-finish-order",
    templateUrl: "./finish-order.component.html",
    styleUrls: ["./finish-order.component.scss"],
})
export class FinishOrderComponent implements OnInit {
    order: any;

    orderArticles: any;

    price: any;

    priceHt: any;

    constructor(
        private orderService: OrderService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.route.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.order = this.router.getCurrentNavigation().extras.state.order;
                this.orderArticles = this.router.getCurrentNavigation().extras.state.orderArticles;
                this.price = this.router.getCurrentNavigation().extras.state.price;
                this.priceHt = this.router.getCurrentNavigation().extras.state.priceHt;
            }
        });
    }

    ngOnInit() {}

    pay(
        address: any,
        additionalAddress: any,
        city: any,
        postalCode: any,
        deliveryDate: any,
        hours: any,
        commentary: any
    ): void {
        let formData = {
            "address": address,
            "additionalAddress": additionalAddress,
            "city": city,
            "postalCode": postalCode,
            "deliveryDate": deliveryDate,
            "priceTotalTTC": this.price.toFixed(2),
            "priceTotalHT": this.priceHt.toFixed(2),
            "hours": hours,
            "commentary": commentary,
        };
        console.log(formData);

        this.orderService
            .updateOrder(formData, this.order.id)
            .subscribe((res) => {
                let navigationExtras: NavigationExtras = {
                    state: {
                        order: this.order,
                        orderArticles: this.orderArticles,
                        price: this.price,
                        priceHt: this.priceHt,
                    },
                };
                this.router.navigate(["paiement"], navigationExtras);
            });
    }
}
