import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/components/user.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';

let token = Cookie.get("token");

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'auth': token })
};

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  apiUrl = 'http://localhost:3000/';

  constructor(private http: HttpClient, private route: ActivatedRoute, private userService: UserService) { }

  getCategories(): Observable<any[]> {
    return this.http.get<any[]>(this.apiUrl + 'category', httpOptions);
  }

}

