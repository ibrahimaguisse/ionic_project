import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/components/user.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';

let token = Cookie.get("token");

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'auth': token })
};

@Injectable({
  providedIn: 'root'
})

export class PaiementService {

  apiUrl = 'http://localhost:3000/card';

  constructor(private http: HttpClient, private route: ActivatedRoute, private userService: UserService) { }

  newCard(formData:any): Observable<any> {
    return this.http.post(this.apiUrl, formData, httpOptions);
  }

}
