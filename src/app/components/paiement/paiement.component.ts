import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { PaiementService } from "./paiement.service";
import { OrderService } from '../order/order.service';

@Component({
    selector: "app-paiement",
    templateUrl: "./paiement.component.html",
    styleUrls: ["./paiement.component.scss"],
})
export class PaiementComponent implements OnInit {
    order: any;

    orderArticles: any;

    price: any;

    priceHt: any;

    constructor(
        private paiementService: PaiementService,
        private router: Router,
        private route: ActivatedRoute,
        private orderService: OrderService
    ) {
        this.route.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.order = this.router.getCurrentNavigation().extras.state.order;
                this.orderArticles = this.router.getCurrentNavigation().extras.state.orderArticles;
                this.price = this.router.getCurrentNavigation().extras.state.price;
                this.priceHt = this.router.getCurrentNavigation().extras.state.priceHt;
            }
        });
    }

    ngOnInit() {}

    pay(name: any, cardNumber: any, expiration: any, security: any): void {

        let formData = {
            "name": name,
            "cardNumber": cardNumber,
            "expiration": expiration,
            "security": security,
            "orderId": this.order.id,
        };

        this.paiementService.newCard(formData).subscribe((res) => {
            //this.router.navigate(["commandes"]);
            // Notif push
        });

        let formDataOrder = {
          "status":"Payée"
        }

        this.orderService.updateOrder(formDataOrder, this.order.id).subscribe((res) => {
          //this.router.navigate(["commandes"]);
          // Notif push
        });

    }
}
