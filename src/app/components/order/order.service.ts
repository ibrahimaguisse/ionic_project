import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/components/user.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';

let token = Cookie.get("token");

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'auth': token })
};
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  
  constructor(private http: HttpClient, private route: ActivatedRoute, private userService: UserService) { }

  apiUrl = 'http://localhost:3000/';

  getOrderDraftByUser(id: any): Observable<any> {
    return this.http.get(this.apiUrl + 'order/draftByUser/'+id, httpOptions);
  }

  getOrder(id: any): Observable<any> {
    return this.http.get(this.apiUrl + 'order/'+id, httpOptions);
  }

  addOrderArticles(formData: any): Observable<any> {
    return this.http.post(this.apiUrl + 'orderArticles', formData, httpOptions);
  }

  addOrder(formData: any): Observable<any> {
    return this.http.post(this.apiUrl + 'order', formData, httpOptions);
  }

  deleteOrderArticles(id: any): Observable<any> {
    return this.http.delete(this.apiUrl + 'orderArticles/'+id, httpOptions);
  }

  editOrderArticles(formData: any, id: any): Observable<any> {
    return this.http.patch(this.apiUrl + 'orderArticles/'+id, formData, httpOptions);
  }

  updateOrder(formData:any, id:any): Observable<any> {
    return this.http.patch(this.apiUrl + 'order/'+id, formData, httpOptions);
  }

}
