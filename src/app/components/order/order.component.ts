import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {

  orders:any[];

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    let copyUser = this.userService.user;
    copyUser.orders.map(function(e){
      var date = new Date(e.deliveryDate);
      e.deliveryDate = date.toISOString().split('T')[0] 
    });
    this.orders = copyUser.orders.filter(order => order.status != 'Brouillon');
  }

  getOrder(order:any){
    let navigationExtras: NavigationExtras = {
      state: {
          order: order,
      },
    };
    this.router.navigate(["orders/" + order.id], navigationExtras);
  }

}
