import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router/src/router_state';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router/src/interfaces';
import { UserService } from 'src/app/components/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardServiceService implements CanActivate {

  isLogin: boolean;

  constructor(private router: Router, private userService: UserService) {

  }

  canActivate(route: ActivatedRouteSnapshot): boolean {

      if (!this.isLogin) {
        return false;
      }

      return true;

  }

}
