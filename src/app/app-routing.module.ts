import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from 'src/app/components/login/login.component';
import { RegisterComponent } from 'src/app/components/register/register.component';
import { HomeComponent } from 'src/app/components/home/home.component';
import { ArticlesComponent } from 'src/app/components/articles/articles.component';
import { ArticleComponent } from 'src/app/components/article/article.component';
import { PanierComponent } from 'src/app/components/panier/panier.component';
import { FinishOrderComponent } from 'src/app/components/finish-order/finish-order.component';
import { AuthGuardServiceService } from 'src/app/auth-guard-service.service';
import { PaiementComponent } from './components/paiement/paiement.component';
import { LoginGuardService } from './login-guard.service';
import { ProfilComponent } from './components/profil/profil.component';
import { OrderComponent } from './components/order/order.component';
import { OrdersArticlesComponent } from './components/orders-articles/orders-articles.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoginGuardService]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [LoginGuardService]
  },
  { path: 'articles',
    component: ArticlesComponent ,
    canActivate: [AuthGuardServiceService],
  },
  { path: 'articles/:id',
    component: ArticleComponent ,
    canActivate: [AuthGuardServiceService],
  },
  { path: 'panier',
    component: PanierComponent ,
    canActivate: [AuthGuardServiceService],
  },
  { path: 'finishOrder',
    component: FinishOrderComponent ,
    canActivate: [AuthGuardServiceService],
  },
  { path: 'paiement',
    component: PaiementComponent ,
    canActivate: [AuthGuardServiceService],
  },
  { path: 'orders',
    component: OrderComponent ,
    canActivate: [AuthGuardServiceService],
  },
  { path: 'orders/:id',
    component: OrdersArticlesComponent ,
    canActivate: [AuthGuardServiceService],
  },
  { path: 'profil',
    component: ProfilComponent ,
    canActivate: [AuthGuardServiceService],
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
