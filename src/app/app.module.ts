import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { LoginComponent } from "src/app/components/login/login.component";
import { RegisterComponent } from "src/app/components/register/register.component";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { HomeComponent } from "src/app/components/home/home.component";
import { ArticlesComponent } from "./components/articles/articles.component";
import { ArticleComponent } from "./components/article/article.component";
import { ReactiveFormsModule } from "@angular/forms";
import { PanierComponent } from "./components/panier/panier.component";
import { FinishOrderComponent } from "./components/finish-order/finish-order.component";
import { PaiementComponent } from './components/paiement/paiement.component';
import { ProfilComponent } from './components/profil/profil.component';
import { OrderComponent } from './components/order/order.component';
import { OrdersArticlesComponent } from './components/orders-articles/orders-articles.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        HomeComponent,
        ArticlesComponent,
        ArticleComponent,
        PanierComponent,
        FinishOrderComponent,
        PaiementComponent,
        ProfilComponent,
        OrderComponent,
        OrdersArticlesComponent
    ],
    entryComponents: [],
    imports: [
        BrowserModule,
        FormsModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
